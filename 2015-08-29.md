## Work - Session 3

### More on floats -

1. How text **flows** around floated elements - http://jsfiddle.net/hojycvyw/

### Task 3.1 

Design home page for national voters' services system. Design it as shown in the below image. Please select an appropriate background image for the website. 

![National voter services](img/national-voter-services.png)

### Task 3.2

There is a new restaurant opening at Hyderabad. It has a website to show the list of the menu items available. 
Design the home page of the website. The design should be as shown in the image.  


![First task](img/hotel-menu.png)


**1** - Header (It should show the hotel name in the middle) 

**2**- Footer (It should show the links as shown in the image)

**3** - Main  container of the website.

**4** - It should have the image of the hotel.

**5** - It should have the list of food items available in the hotel.

**6** - This is a button to register. To use the services of the hotel (make an order for door delivery) the customer has to register with his details. 

**NOTES :**

* The sections 4, 5 should be of same height and width.
* The Register button should be positioned exactly to the center of the main container.
* Use the same colors as shown in the image.
* Use white color for the font.

### Task 3.3

Design the layout as shown in the below image. Use the same colors. Use float left and right to align the containers. 

![Task 3.3](img/task3.3.png)